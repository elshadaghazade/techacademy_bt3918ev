somelist = ["al", "allow", "abba", "face", "wow", "46784", "public", 134]

# somelist siyahısında olan sətirlərin içərisindən uzunluğu 2 simvoldan çox olan, əvvəli və sonu eyni hərflə bitən sözləri print edin
# ================================================

# ================================================

# Verilmiş siyahıda olan təkrarlanmaları silin
# ================================================
somelist = [1,2,3,2,12,2,4,3,10,3,2,1]
# ================================================

# Verilmiş siyahının elementlərindən bəziləri siyahıdır. Yoxlayın əgər o siyahılardan boş olanı varsa onları ana siyahıdan silin
# ================================================
somelist = [
    1,
    2,
    [],
    3, 
    [],
    [1,2,3],
    4,
    "TechAcademy",
    ["T", "A"],
    []
]
# ================================================