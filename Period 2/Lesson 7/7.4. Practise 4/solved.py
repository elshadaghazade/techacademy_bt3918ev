# list comprehension vasitəsi ilə uzunluğu 20 olan, elementlərinin dəyəri 1000-dən böyük olmayan və elementləri random rəqəmlər olan siyahı yaradın
# ==============================================
from random import randint
numlist = [randint(1, 1000) for x in range(20)]
# ==============================================

# yaradılan siyahının elementləri içərisindən yalnızca tək ədəd olanlardan yeni siyahı yaradın və print edin. Lakin bunu bir sətirlik kodla edin
# ==============================================
newlist = [i for i in numlist if i%2]
print(newlist)
# ==============================================

# yaradılan siyahının əsasında yeni siyahı yaradın, lakin yeni siyahının elementləri əvvəlki siyahının elementlərinin kvadratı olmalıdır, və əgər elementin kvadratı 6-ya bölünürsə o zaman yeni siyahıya daxil edilməlidir
# ==============================================
newlist = [i**2 for i in numlist if not (i**2)%6]
print(newlist)
# ==============================================