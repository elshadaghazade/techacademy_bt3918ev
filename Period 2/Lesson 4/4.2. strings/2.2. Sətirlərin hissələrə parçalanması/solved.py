# -*- coding: utf-8 -*-

myString = "SALAM DÜNYA"

# 1. myString[<start>:<end>:step] üsulundan istifadə edərək Ü hərfini çap edin
print("1. myString[6:7] =>", myString[7:8])

# 2. myString[<start>:<end>:step] üsulundan istifadə edərək SALAM cümləsini a dəyişəninə, DÜNYA sözünü b dəyişəninə mənimsədərək hər birini ekranda çap edin
a = myString[:5]
b = myString[6:]

print("2. a =>", a, "; b =>", b)

# 3. myString[<start>:<end>:step] üsulundan istifadə edərək SALAM DÜNYA cümləsini tərsinə ekranda çap edin
print("3. myString[::-1] =>", myString[::-1])

# 4. myString[<start>:<end>:step] üsulundan istifadə edərək SALAM DÜNYA cümləsindən birinci simvol daxil olmaqla hər 3-cü simvolu çap edin
print("4. myString[::3] =>", myString[::3])

# 5. myString[<start>:<end>:step] üsulundan istifadə edərək SALAM DÜNYA cümləsinin hər 2-ci simvolunu, elə 2-ci simvoldan başlamaq şərti ilə çap edin
print("5. myString[2::2] =>", myString[2::2])

# 6. myString[<start>:<end>:step] üsulundan istifadə edərək SALAM DÜNYA cümləsinin axırıncı simvol istisna olmaqla son 3 simvolunu ekranda çap edin
print("6. myString[-4:-1] =>", myString[-4:-1])