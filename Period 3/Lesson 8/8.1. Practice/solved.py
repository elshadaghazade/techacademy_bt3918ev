from array import array
import sys
from random import randint

# 5 elementli unsigned integer massivi yaradan və bütün elementlərini print edən proqram yazın. Massivin ayrı ayrı elementlərini indeksin köməkliyi ilə print edin
# =========================================================
myarray = array('I', [1,2,3,3,4,5])
print(myarray)

for i in range(myarray.buffer_info()[1]):
    print(myarray[i])
# =========================================================

# massivin sonuna yeni element artıran proqram yazın
# =========================================================
print("Original array is:", myarray)
myarray.append(154)
print("After appending new item myarray is:", myarray)
# =========================================================

# massivin bütün elementlərini tərsinə çevirən proqram yazın
# =========================================================
print("Original array is:", myarray)
myarray.reverse()
print("After reversing myarray is:", myarray)
# =========================================================


# Massivin tək elementinin ölçüsünü baytla göstərin
# =========================================================
print("length of item is:", myarray.itemsize, "bytes")
# =========================================================

# massivin yaddaşdakı adresini və elementlərinin sayını print edin. Daha sonra elementlərin buferdə nə qədər yer tutduğunu print edin
# =========================================================
print("Buffer info of myarray is:", myarray.buffer_info())
print("size of elements in memory of myarray is:", myarray.itemsize * myarray.buffer_info()[1], "bytes")
print("Real size of myarray object is:", sys.getsizeof(myarray), "bytes")
# =========================================================

# 3 rəqəminin massivdə neçə dəfə təkrarlandığını göstərin
# =========================================================
print("number 3 occurs in the array", myarray.count(3), "times")
# =========================================================

# massivin sonuna aşağıdakı siyahını artırın və print edin
# =========================================================
newlist = [4,5,6,7]
myarray.extend(newlist)
print("myarray is:", myarray)
# =========================================================

# massivi bytes massivinə çevirin
# =========================================================
mybytes = myarray.tobytes()
print(mybytes)
# =========================================================

# xüsusi siyahını massivin sonuna artırın, lakin dəyərlər uyğun gəlmədikdə xəta mesajı çıxmasın
# =========================================================
mylist = [randint(0, 255) for i in range(1000)]
myarray.fromlist(mylist)
print(myarray)
# =========================================================

# massivin indeksi 2 olan elementindən sonraya 300 rəqəmi əlavə edin
# =========================================================
myarray.insert(3, 300)
print(myarray)
# =========================================================

# massivin hər hansı bir elementini indeksinə görə silin
# =========================================================
myarray.pop(3)
print(myarray)
# =========================================================

# massivin hər hansı bir elementini onun dəyərinə görə silin
# =========================================================
myarray.remove(50)
print(myarray)
# =========================================================

# massivi adi siyahıya çevirin
# =========================================================
mylist = myarray.tolist()
print(myarray)
# =========================================================